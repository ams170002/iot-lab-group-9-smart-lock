import RPi.GPIO as GPIO
import mariadb
import datetime
import time
import sys
from pn532 import *

class NFCReader:
    def __init__(self, dbconn=None):
        pass

    def read_nfc(self):
        try:
            # Communication via Serial Peripheral Interface
            self.pn532 = PN532_SPI(debug=False, reset=20, cs=4)
        except Exception as e:
            print(f"Error: {e}")

        try:
            # Configure PN532 to communicate with MiFare cards
            self.pn532.SAM_configuration()

            print('Waiting for RFID/NFC card...')
            # Keep looping until an NFC card is read
            while True:
                uid = self.pn532.read_passive_target(timeout=0.5)
                if uid is None:
                    continue
                print('Found card with UID:', [hex(i) for i in uid])
                uid_string = "_".join([str(b) for b in uid])
                GPIO.cleanup()
                # Return uid of scanned card as a string
                return uid_string
        except Exception as e:
            print(f"Error while configuring/reading NFC card(s): {e}")

if __name__ == '__main__':
    nfc_reader = NFCReader()





