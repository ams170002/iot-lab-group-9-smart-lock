import face_recognition
import picamera
import numpy as np
import PIL
import os
import time
import threading

class FaceRecognizer():
    def __init__(self):
        self.video_feed = np.empty((240, 320, 3), dtype=np.uint8)
        self.face_data_path = os.path.join(os.getcwd(), 'face_data')
        self.pictures_path = os.path.join(self.face_data_path, 'pictures')
        self.encodings_path = os.path.join(self.face_data_path, 'encodings')
        if not os.path.isdir(self.face_data_path):
            os.makedirs(self.face_data_path)
            print("Created directory for face data")
        if not os.path.isdir(self.pictures_path):
            os.makedirs(self.pictures_path)
            print("Created directory for face pictures")
        if not os.path.isdir(self.encodings_path):
            os.makedirs(self.encodings_path)
            print("Created directory for face encodings")
    
    def create_encodings(self):
        for picture in os.listdir(self.pictures_path):
            if not picture.endswith('.jpg'):
                continue
            name = picture[0:picture.find('.')]
            encoding_file = os.path.join(self.encodings_path, name+'.csv')
            image = face_recognition.load_image_file(os.path.join(self.pictures_path,picture))
            encoding = face_recognition.face_encodings(image)[0]
            np.savetxt(encoding_file, np.asarray(encoding))

    def find_encoding_user(self, encoding):
        for encoding_file in os.listdir(self.encodings_path):
            if not encoding_file.endswith('.csv'):
                continue
            f_encoding = np.loadtxt(os.path.join(self.encodings_path, encoding_file))
            if all(np.isclose(encoding, f_encoding)):
                user_id = encoding_file.replace("user_","")
                user_id = user_id.replace(".csv","")
                return user_id
        print("Could not find a user with the corresponding face...")
        return None

    def take_picture_of_face(self, user_id=-1):
        face_locations = []
        print("Getting ready to take picture...\nPlace face within view of the camera")
        with picamera.PiCamera() as camera:
            camera.resolution = (320,240)
            camera.start_preview()
            while True:
                camera.capture(self.video_feed, format="rgb")
                face_locations = face_recognition.face_locations(self.video_feed)
                if len(face_locations) > 0:
                    if user_id != -1:
                        file_name = "user_" + str(user_id) + ".jpg"
                    else:
                        file_name = "user_NO_USER_ID.jpg"
                    pic_path = os.path.join(self.face_data_path, 'pictures', file_name)
                    im = PIL.Image.fromarray(self.video_feed)
                    im.save(pic_path)
                    self.create_encodings()
                    break

    def detect_face(self):
        face_locations = []
        face_encodings = []
        known_face_encodings = []
        
        for encoding_file in os.listdir(self.encodings_path):
            if not encoding_file.endswith('.csv'):
                continue
            encoding = np.loadtxt(os.path.join(self.encodings_path, encoding_file))
            known_face_encodings.append(encoding)

        if len(known_face_encodings) < 1:
            print("There are no known faces in the system. Quitting face scan...")
            return None

        self.scanning = True
        timer_thread = threading.Thread(target=self._camera_timer, args=(0,))
        timer_thread.start()
        print("Ready to scan for faces...")

        with picamera.PiCamera() as camera:
            camera.resolution = (320, 240)
            camera.start_preview()

            while self.scanning:
                camera.capture(self.video_feed, format="rgb")
                face_locations = face_recognition.face_locations(self.video_feed)
                if len(face_locations) < 1:
                    continue
                face_encodings = face_recognition.face_encodings(self.video_feed, face_locations)

                for face_encoding in face_encodings:
                    matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

                    face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
                    best_match_idx = np.argmin(face_distances)
                    if matches[best_match_idx]:
                        matched_encoding = known_face_encodings[best_match_idx]
                        return matched_encoding
            camera.stop_preview()
        return None
    
    def _camera_timer(self,timer):
        time_sec = timer
        while time_sec > 0 and self.scanning:
            time.sleep(1)
            time_sec = time_sec - 1
        if timer > 0:
            self.scanning = False
