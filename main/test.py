import sys
import mariadb
try:
    conn = mariadb.connect(
        user="root",
        # password="maria",               #mariadb
        # host="localhost",              #mariadb
        password="awssmartlock",        #AWS
        host="smartlock.cutobaul1wel.us-east-2.rds.amazonaws.com",            #AWS
        port=3306,      
        # database="smartlock"              #mariadb
        database="smartlock"         #AWS
    )
    print("Successfully connected to the database")
except mariadb.Error as e:
    print(f"Error: {e}")
    sys.exit()