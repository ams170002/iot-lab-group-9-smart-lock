import smtplib
import mariadb
import sys
import threading
import I2C_LCD_driver       # A pre-written script to interface with an LCD driver via I2C (credit in I2C_LCD_driver.py header)
from time import *

import ft
import nfc
import face_rec

gmail_user = 'raspberrypi.IoTSmartLock@gmail.com'
gmail_password = 'PokemonRaspberry'

sender = 'raspberrypi.IoTSmartLock@gmail.com'
receivers = ['raspberrypi.IoTSmartLock@gmail.com']

class LockMenu:
    def __init__(self):
        """ Initialize connection to the database and the sensors
        """
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_display_string("Welcome to", 1)
        mylcd.lcd_display_string("IoT SmartLock", 2)
        sleep(2)
        try:
            self.conn = mariadb.connect(
                user="root",
                # password="maria",               #mariadb
                # host="localhost",              #mariadb
                password="awssmartlock",        #AWS
                host="smartlock.cutobaul1wel.us-east-2.rds.amazonaws.com",            #AWS
                port=3306,      
                # database="smartlock"              #mariadb
                database="smartlock"         #AWS
            )
            print("Successfully connected to the database")
        except mariadb.Error as e:
            print(f"Error: {e}")
            sys.exit()
        self.cur = self.conn.cursor()
        self.cur.execute("SHOW TABLES")
        # print(self.cur.fetchall())
        self.fpreader = ft.FingerprintReader()
        #print("FP READY")
        self.nfc = nfc.NFCReader()
        #print("NFC READY")
        self.face_reader = face_rec.FaceRecognizer()
        #print("FaceRecognizer READY")

        self.is_enrolling = False

    def enroll_user(self):
        """Take user through the process of adding their fingerprint and their NFC card to the database
        """
        print("NOW ENROLLING A NEW USER!")
        mylcd = I2C_LCD_driver.lcd()
        mylcd.lcd_display_string("NOW ENROLLING!", 1)
        mylcd.lcd_display_string("Scan NFC card...", 2)
        self.is_enrolling = True
        nfcuid = self.nfc.read_nfc()
        templates = self.fpreader.get_templates()
        fploc = -1
        for i in range (1, 128):
            if i not in templates:
                fploc = i
                break
        if fploc == -1:
            print("There is no more space to add a fingerprint!!!!")
            self.is_enrolling = False
            mylcd.lcd_display_string("ENROLL ABORTED!", 1)
            mylcd.lcd_display_string("RAN OUT OF STORAGE!", 2)
            return
        mylcd.lcd_display_string("NOW ENROLLING!", 1)
        mylcd.lcd_display_string("Scan finger...", 2)
        fplocation, fpdata = self.fpreader.enroll_finger(fploc)
        mylcd.lcd_display_string("NOW ENROLLING!", 1)
        mylcd.lcd_display_string("Scan face...", 2)
        self.face_reader.take_picture_of_face(nfcuid)
        print("Face scanned successfully!")
        self.cur.execute("INSERT INTO users(uid, fpid, fpdata) VALUES(%s,%s,%s)", (nfcuid, fplocation, fpdata))
        self.conn.commit()
        self.is_enrolling = False
        mylcd.lcd_display_string("ENROLL COMPLETE!", 1)
        mylcd.lcd_display_string("                ", 2)

        message = """From: From IoT SmartLock <raspberrypi.IoTSmartLock@gmail.com>
        To: To Liam <raspberrypi.IoTSmartLock@gmail.com>
        Subject: IoT Smart Lock Update: Registration\n\n
        """
        message += f"IoT SmartLock has successfully registered user {nfcuid}"

        try:
            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            server.ehlo()
            server.login(gmail_user, gmail_password)
            server.sendmail(sender, receivers, message)
            server.close()
            print("Email sent")
        except Exception as e:
            print(e)

    def login_user(self):
        """Process of logging in the user by scanning their NFC card and fingerprint
        """
        if not self.is_enrolling:
            mylcd = I2C_LCD_driver.lcd()
            mylcd.lcd_display_string("Scan NFC card", 1)
            mylcd.lcd_display_string("Locked [] [] ", 2)
            read_uid = self.nfc.read_nfc()
            print(read_uid)
            uid_found = False
            self.cur.execute("SELECT uid, fpid, fpdata FROM users WHERE uid=?", (read_uid,))
            for uid, fploc, fpdata in self.cur:
                print(f"uid: {uid}")
                if(read_uid == uid):
                    print("uid found")
                    mylcd.lcd_display_string("uid found", 1)
                    mylcd.lcd_display_string("Locked  [x] [] ", 2)
                    uid_found = True
                    user_row = (uid, fploc, fpdata)
                    break
            if not uid_found:
                print("uid not found. Calling security")
                mylcd.lcd_display_string("ACCESS DENIED   ", 1)
                mylcd.lcd_display_string("Locked [] []  ", 2)
            if uid_found:
                mylcd.lcd_display_string("Scan fingerprint", 1)
                mylcd.lcd_display_string("Locked [x] []  ", 2)
                sleep(5)
                fpres = True
                fpres = self.fpreader.scan()
                if fpres == user_row[1]:
                    ## FACE DETECTION
                    face_encoding = self.face_reader.detect_face()
                    user_id = self.face_reader.find_encoding_user(face_encoding)
                    print(f"ID of user detected with camera: {user_id}")
                    if user_id == uid:
                        print("CORRECT FACE WAS DETECTED!!!")
                        print("ACCESS GRANTED  ")
                        mylcd.lcd_display_string("ACESS GRANTED   ", 1)
                        mylcd.lcd_display_string("Unlocked [x] [x]", 2)
                        message = """From: From IoT SmartLock <raspberrypi.IoTSmartLock@gmail.com>
                        To: To Liam <raspberrypi.IoTSmartLock@gmail.com>
                        Subject: IoT Smart Lock Update: Login Attempt - Success\n\n
                        """
                        message += f"User {uid} has successfully logged in!"

                        try:
                            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
                            server.ehlo()
                            server.login(gmail_user, gmail_password)
                            server.sendmail(sender, receivers, message)
                            server.close()
                            print("Email sent")
                        except Exception as e:
                            print(e)
                    else:
                        print("Scanned face is not correct!")
                        mylcd.lcd_display_string("ACCESS DENIED   ", 1)
                        mylcd.lcd_display_string("Locked [x] []", 2)
                else:
                    print("Fingerprint is not correct!")
                    mylcd.lcd_display_string("ACCESS DENIED   ", 1)
                    mylcd.lcd_display_string("Locked [x] []", 2)

if __name__ == '__main__':
    lock = LockMenu()
    user_in = input("Do you want to:\na) register a new user\nb) unlock the lock\nc) exit\nInput: ")
    while user_in != 'c':
        if user_in == 'a':
            lock.enroll_user()
        elif user_in == 'b':
            lock.login_user()
        user_in = input("Do you want to:\na) register a new user\nb) unlock the lock\nc) exit\nInput: ")
    
    print("Thank you for using IOT Smart Lock!")
