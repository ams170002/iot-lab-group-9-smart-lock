# Code based on example code provided by Adafruit Industries

# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import datetime
import board
import mariadb
from digitalio import DigitalInOut, Direction
import adafruit_fingerprint
import sys

led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

import serial
uart = serial.Serial("/dev/ttyS0", baudrate=57600, timeout=1)

##################################################

class FingerprintReader():
    def __init__(self):
        self.finger = adafruit_fingerprint.Adafruit_Fingerprint(uart)

    def run_menu(self):
        while True:
            print("----------------")
            if self.finger.read_templates() != adafruit_fingerprint.OK:
                raise RuntimeError("Failed to read templates")
            print("Fingerprint templates:", self.finger.templates)
            print("e) enroll print")
            print("f) find print")
            print("d) delete print")
            print("u) update database")
            print("c) check database")
            print("x) exit")
            print("----------------")
            c = input("> ")

            if c == "e":
                self.enroll_finger(self.get_num())
            if c == "f":
                if self.get_fingerprint():
                    print("Detected #", self.finger.finger_id, "with confidence", self.finger.confidence)
                else:
                    print("Finger not found")
            if c == "d":
                if self.finger.delete_model(self.get_num()) == adafruit_fingerprint.OK:
                    print("Deleted!")
                else:
                    print("Failed to delete")
            if c == "u":
                self.update_database()
            if c == "x":
                exit()


    def get_templates(self):
        """Return list of locations in sensor's memory that contain a scanned fingerprint
            Returns:
                self.finger.templates (list<int>): list of template indices containing a fingerprint scan
        """
        if self.finger.read_templates() != adafruit_fingerprint.OK:
            raise RuntimeError("Failed to read templates")
        return self.finger.templates

    def get_data(self, loc):
        """Get the serialized data of the fingerprint from a given location on the sensor's template memory
            Parameters:
                loc (int): template index to look for
            Returns:
                str (string): serialized fingerprint data at loc
        """
        if self.finger.read_templates() != adafruit_fingerprint.OK:
            raise RuntimeError("Failed to read templates")
        loc_list = self.finger.templates

        if loc not in loc_list:
            print(f"There is no fingerprint stored in location {loc}...")
            return

        self.finger.load_model(loc)
        data = self.finger.get_fpdata(slot=1)
        str = ""
        for d in data:
            str += hex(d)
        return str
        #return [hex(d) for d in data]

    def scan(self):
        """Scan a fingerprint and detect if it exists in the sensor's. If it does exist, return the location of fingerprint
            Returns:
                self.finger.finger_id (int): location of the scanned fingerprint in the sensor's template memory
        """
        if self.get_fingerprint():
            print("Detected #", self.finger.finger_id, "with confidence", self.finger.confidence)
            return self.finger.finger_id
        else:
            print("Finger not found")
            return -1

    def get_fingerprint(self):
        """Get image of finger placed on sensor and search for it in the sensor's memory
            Returns:
                boolean: True if the scanned finger is in the fingerprint's memory. Otherwise, False.
        """
        print("Waiting for finger...")
        while self.finger.get_image() != adafruit_fingerprint.OK:
            pass
        if self.finger.image_2_tz(1) != adafruit_fingerprint.OK:
            return False
        if self.finger.finger_search() != adafruit_fingerprint.OK:
            return False
        return True

    # pylint: disable=too-many-statements
    def enroll_finger(self, location):
        """Take a 2 finger images and template it, then store in 'location'
            Parameters:
                location (int): index in template memory of sensor to store the new fingerprint
            Returns:
                boolean: Whether the fingerprint want scanned and saved correctly or not
        """
        for fingerimg in range(1, 3):
            if fingerimg == 1:
                print("Place finger on sensor...", end="", flush=True)
            else:
                print("Place same finger again...", end="", flush=True)

            while True:
                i = self.finger.get_image()
                if i == adafruit_fingerprint.OK:
                    print("Image taken")
                    break
                if i == adafruit_fingerprint.NOFINGER:
                    print(".", end="", flush=True)
                elif i == adafruit_fingerprint.IMAGEFAIL:
                    print("Imaging error")
                    return False
                else:
                    print("Other error")
                    return False

            print("Templating...", end="", flush=True)
            i = self.finger.image_2_tz(fingerimg)
            if i == adafruit_fingerprint.OK:
                print("Templated")
            else:
                if i == adafruit_fingerprint.IMAGEMESS:
                    print("Image too messy")
                elif i == adafruit_fingerprint.FEATUREFAIL:
                    print("Could not identify features")
                elif i == adafruit_fingerprint.INVALIDIMAGE:
                    print("Image invalid")
                else:
                    print("Other error")
                return False

            if fingerimg == 1:
                print("Remove finger")
                time.sleep(1)
                while i != adafruit_fingerprint.NOFINGER:
                    i = self.finger.get_image()

        print("Creating model...", end="", flush=True)
        i = self.finger.create_model()
        if i == adafruit_fingerprint.OK:
            print("Created")
        else:
            if i == adafruit_fingerprint.ENROLLMISMATCH:
                print("Prints did not match")
            else:
                print("Other error")
            return False

        print("Storing model #%d..." % location, end="", flush=True)
        i = self.finger.store_model(location)
        if i == adafruit_fingerprint.OK:
            print("Stored")
            return location, self.get_data(location)
        else:
            if i == adafruit_fingerprint.BADLOCATION:
                print("Bad storage location")
            elif i == adafruit_fingerprint.FLASHERR:
                print("Flash storage error")
            else:
                print("Other error")
            return None, None

        return None, None
        
    def get_num(self):
        """Use input() to get a valid number from 1 to 127. Retry till success!"""
        i = 0
        while (i > 127) or (i < 1):
            try:
                i = int(input("Enter ID # from 1-127: "))
            except ValueError:
                pass
        return i


def main():
    fp = FingerprintReader()
    fp.run_menu()

if __name__ == "__main__":
    main()
